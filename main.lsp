
(load 
    (merge-pathnames "../sc/src/all.lsp" *load-truename*))
(in-package :sc)
(load 
    (merge-pathnames "utility.lsp" *load-truename*))
(load 
    (merge-pathnames "theory/event-stuff.lsp" *load-truename*))
(load 
    (merge-pathnames "theory/scales.lsp" *load-truename*))
(load 
    (merge-pathnames "theory/electronics.lsp" *load-truename*))
(load 
    (merge-pathnames "theory/rand.lsp" *load-truename*))
(load 
    (merge-pathnames "theory/time-manager.lsp" *load-truename*))
(load 
    (merge-pathnames "practice/string-instrument-player.lsp" *load-truename*))
(load 
    (merge-pathnames "practice/guitar-player.lsp" *load-truename*))
(load 
    (merge-pathnames "practice/bass-player.lsp" *load-truename*))
(load 
    (merge-pathnames "practice/drumset-player.lsp" *load-truename*))
(load 
    (merge-pathnames "practice/explosion-player.lsp" *load-truename*))
(load 
    (merge-pathnames "practice/piano-player.lsp" *load-truename*))

;personal notes:
;PLAY MIDI FILE AT 150BPM
;0.15 for 2 notes at 150bpm is okay with halftime for getup

;piano not as simulation of an orchestra but of a band
;additive rhythms realized with l-systems
;other forms of melodies through different underlying graphs
;valid midinotes on playerpiano: 21 to 108
;under 36 no chords
;13 hits per second not wonky on yamaha disklavier

;structure:
;scale1
;long build up:
;triggered fadeins very high notes
;number of notes, fadein-unit, duration of end-note guitar
;robotic arpeggios middle
;louder drums
;at half break - postpunkbassline kicks in bass

;with pedal
;climax 21 22 23 thunderbuildup
;lightning high notes
;reversed fadeins?
;cymbal build upat theend

;scale2
;resolution in as much notes as possible chord
;"beautiful" calm outro kitchklavier
;velocity 1 hammer noises as background

(defvar +root-note+ 24)

(defvar +scale1+ 
    (scale1 +root-note+))
(print "scale1:")
(print +scale1+)
(print "length of scale1:")
(print 
    (length +scale1+))

(defvar +scale2+ 
    (scale2 +root-note+))
(print "scale2:")
(print +scale2+)
(print "length of scale2:")
(print 
    (length +scale2+))

(defvar +scale3+ 
    (scale3 +root-note+))
(print "scale3:")
(print +scale3+) 
(print "length of scale3:")
(print 
    (length +scale3+))

(defvar +melody+ '
    (2 0 5 3))
(defvar +scale-trans+ 
    (list
        (lambda 
            (x) 
            (swap-every-nth x 4))
        (lambda 
            (x) 
            (swap-every-nth x 5))
        (lambda 
            (x) 
            (swap-every-nth x 6))
        (lambda 
            (x) 
            (swap-if x 'oddp))
        (lambda 
            (x) 
            (mirror-at x 8))
        (lambda 
            (x) 
            (mirror-at x 10))
        (lambda 
            (x) 
            (mirror-at x 12))
        (lambda 
            (x) 
            (swap-if x 'evenp))))

;instruments
(defvar +guitar+ 
    (make-guitar-player +scale1+ +melody+))
(defvar +bass+ 
    (make-bass-player +scale1+ +melody+))
(defvar +drumset+ 
    (make-instance 'drumset-player :note-scale +scale3+ :min-dur 0.05))
(defvar +explosion+ 
    (make-instance 'explosion-player :note-scale +scale1+))
(defvar +piano+ 
    (make-piano-player +scale2+ +melody+))

(defvar +time-manager+ 
    (make-instance 'time-manager))

(print "piece starts here")
;soundcheck
(print "soundcheck")
(defvar +bass-check-events+ 
    (get-bassline +bass+ 1 
        (make-cscl '
            (127))))
(defvar +kick-check-events+ 
    (get-kick-pattern +drumset+ 
        (make-cscl '
            (127)) 2 
        (make-cscl '
            (0.95))))
(defvar +hihat-check-events+ 
    (get-hihat-pattern +drumset+ 
        (make-cscl '
            (127)) 0.5 
        (make-cscl '
            (0.25))))
(defvar +snare-check-events+ 
    (get-snare-pattern +drumset+ 
        (make-cscl '
            (127)) 2 
        (make-cscl '
            (0.9))))
(defvar +soundcheck-events+ 
    (append +bass-check-events+ +kick-check-events+ +hihat-check-events+ +snare-check-events+))

(add-events +time-manager+ 
    (list +soundcheck-events+))

;intro
(print "intro")
(defvar +intro-events+ 
    (get-intro +guitar+ 128 
        (make-cscl '
            (20 30 40 60 80 90 100)) 
        (make-cscl '
            (0.25 0.5 0.8 0.5 0.25))))

(add-events +time-manager+ 
    (list +intro-events+))

;part1
(print "part1")
(defvar +durs-part1+ '
    (64 32 16 17 8 9 4 2))
(defvar +amps-part1+ 
    (make-cscl '
        (127 120 115 110 115 120 127)))
(defvar +rest-factors-part1+ 
    (make-cscl '
        (0.9 0.8 0.7 0.5 0.45 0.4)))
(defvar +guitar1-events+ 
    (loop for trans in +scale-trans+ for dur in +durs-part1+ for i from 1 to 
        (length +durs-part1+) append
        (if 
            (< i 5)
            (progn
                (transform-scale +guitar+ trans)
                (get-line +guitar+ dur +amps-part1+ 
                    (make-cscl '
                        (0.5))))
            (progn
                (transform-scale +guitar+ trans)
                (get-line +guitar+ dur +amps-part1+ +rest-factors-part1+)))))

(defvar +bass1-events+ 
    (loop for trans in +scale-trans+ for dur in +durs-part1+ append
        (progn
            (transform-scale +bass+ trans)
            (get-bassline +bass+ dur +amps-part1+))))

(defvar +kick1-events+ 
    (loop for dur in +durs-part1+ for i from 1 to 
        (length +durs-part1+) append
        (if 
            (< i 5)
            (get-kick-pattern +drumset+ +amps-part1+ dur 
                (make-cscl '
                    (0.95)))
            (get-kick-pattern +drumset+ +amps-part1+ dur +rest-factors-part1+))))

(defvar +hihat1-events+ 
    (loop for dur in +durs-part1+ for i from 1 to 
        (length +durs-part1+) append
        (if 
            (< i 4)
            (get-hihat-pattern +drumset+ +amps-part1+ dur 
                (make-cscl '
                    (0.25)))
            (get-hihat-pattern +drumset+ +amps-part1+ dur +rest-factors-part1+))))

(defvar +snare1-events+ 
    (loop for dur in +durs-part1+ for i from 1 to 
        (length +durs-part1+) append
        (if 
            (< i 3)
            (get-snare-pattern +drumset+ +amps-part1+ dur 
                (make-cscl '
                    (0.9)))
            (get-snare-pattern +drumset+ +amps-part1+ dur +rest-factors-part1+))))

(add-events +time-manager+ 
    (list +guitar1-events+ +bass1-events+ +kick1-events+ +snare1-events+ +hihat1-events+))

;explosion1
;6 and 0.2
;0.3? are the best values possible
(print "explosion")
(defvar +explosion1-events+ 
    (get-explosion1 +explosion+ 48 6 0.3 +drumset+))

(add-events +time-manager+
    (list +explosion1-events+))

;;part2
(print "part2")
(defvar +thunder-events+ 
    (get-thunder +bass+ 768 0.25 0.15 20 100))

(defvar +cymbal-events+ 
    (get-cymbal +drumset+ 127 
        (duration-of-events +thunder-events+)))

(defvar +lightning-events+ 
    (reverse 
        (loop for trans in +scale-trans+ for dur in 
            (scale-proportions +durs-part1+ 
                (duration-of-events +thunder-events+)) append
            (progn
                (transform-scale +guitar+ trans)
                (get-intro +guitar+ dur +amps-part1+ +rest-factors-part1+)))))

(defvar +pedal-part2+ 
    (list 
        (make-pedal-rest 
            (duration-of-events +thunder-events+) 127)))

(add-events +time-manager+
    (list +thunder-events+ +cymbal-events+ +lightning-events+ +pedal-part2+))

;explosion2
(print "explosion2")
(defvar +explosion2-events+ 
    (get-explosion2 +explosion+))

(add-events +time-manager+
    (list +explosion2-events+))

;outro
(print "outro")
(defvar +fib-lens-outro+ '
    (1 2 3 3 4 3 4 4))
(defvar +fib-sizes-outro+ 
    (split-into-sub-groups
        (fibonacci-transitions 
            (reduce '+ +fib-lens-outro+) '
            (8 7 6 5 4 3 2 1))
+fib-lens-outro+))

(defvar +piano-events+ 
    (loop for trans in +scale-trans+ for len in +fib-lens-outro+ for size in +fib-sizes-outro+ append
        (progn
            (transform-scale +piano+ trans)
            (get-midi-events +piano+ len size))))

(defvar +high-sine-events+ 
    (high-sine +guitar+ 
        (duration-of-events +piano-events+) 0.15 100))

;6 0.46 and 15 are the best values possible
(defvar +hammer-events+ 
    (get-hammers +drumset+ 
        (duration-of-events +piano-events+) 6 0.46 15))

(defvar +pedal-outro+ 
    (list 
        (make-pedal-rest 
            (duration-of-events +piano-events+) 127)))

(add-events +time-manager+
    (list +piano-events+ +high-sine-events+ +hammer-events+ +pedal-outro+))

;end
(defvar +silence-events+ 
    (make-midi-event 21 1 64 0 127))

(defvar +hammer2-events+ 
    (get-hammers +drumset+ 
        (duration-of-events +silence-events+) 6 0.46 15))

(add-events +time-manager+ 
    (list +silence-events+ +hammer2-events+))

(event-list-to-midi-file 
    (events +time-manager+) :midi-file "build/gpi01.mid" :auto-open Nil)