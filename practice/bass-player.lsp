(defclass bass-player 
  (string-instrument-player)
  (
    (lfl-thunder :initform 
      (make-l-for-lookup 'l-sys-thunder '
        (
          (1 
            (
              (21)))
          (2 
            (
              (22)))
          (3 
            (
              (23)))) '
        (
          (1 
            (1 2 3))
          (2 
            (1 3 2))
          (3 
            (1 2 3 2)))) :accessor lfl-thunder)
    (repetitions-seeder :initform 
      (make-cscl 
        (loop for i from 1 to 3 collect i)) :accessor repetitions-seeder)
    (lfl-repetitions :initform 
      (make-l-for-lookup 'l-sys-repetitions '
        (
          (1 
            (
              (play)))
          (2 
            (
              (play)))
          (3 
            (
              (play)))
          (4 
            (
              (change)))) '
        (
          (1 
            (1 2 3 1 2 3 4))
          (2 
            (3 2 1 3 2 1))
          (3 
            (1 1 1 1 4))
          (4 
            (4)))) :accessor lfl-repetitions)))

(defun make-bass-player 
  (scale melody)
  (make-instance 'bass-player :lowest-note 28 :highest-note 45 :note-scale scale :degrees melody))

(defmethod get-repetitions 
  (
    (obj bass-player) n)
  (let* 
    (
      (repetitions 
        (do-lookup 
          (lfl-repetitions obj) 
          (get-next 
            (repetitions-seeder obj)) n))
      (sum 0)
      (result  
        (list)))
    (loop for r in repetitions do
      (if 
        (eq r 'change)
        (progn
          (push sum result)
          (push 1 result)
          (setf sum 0))
        (incf sum)))
    (when 
      (< 0 sum)
      (push sum result))
    (reverse result)))

; operation on degrees
(defmethod get-leading2 
  (
    (obj bass-player) current future)
  (cond
    (
      (= 
        (- current 1) future) 
      (+ current 1))
    (
      (= 
        (+ current 1) future) 
      (- current 1))
    (
      (< current future) 
      (+ current 1))
    (
      (> current future) 
      (- current 1))
    (
      (= current future) 
      (- current 2))))

; from the last again to the first
(defmethod get-leadings 
  (
    (obj bass-player) n)
  (let* 
    (
      (future-degrees 
        (append 
          (rest 
            (degrees obj)) 
          (list 
            (first 
              (degrees obj)))))
      (result 
        (loop for future-degree in future-degrees for degree in 
          (degrees obj) collect 
          (get-leading2 obj degree future-degree))))
    (loop-for-n-steps result n)))

(defmethod get-pitches 
  (
    (obj bass-player) n)
  (let* 
    (
      (notes 
        (loop-for-n-steps 
          (apply-scale 
            (degrees obj) 
            (crop-scale obj)) 
          (ceiling 
            (/ n 2.0))))
      (leadings 
        (apply-scale 
          (get-leadings obj 
            (floor 
              (/ n 2.0))) 
          (crop-scale obj)))
      (result 
        (loop for n in notes for l in leadings append 
          (list n l))))
    (if 
      (< 
        (length leadings) 
        (length notes))
      (append result 
        (last notes)) result)))

; duration in 1/8
(defmethod get-bassline 
  (
    (obj bass-player) duration amps)
  (let* 
    (
      (unit 0.5)
      (repetitions 
        (get-repetitions obj 
          (floor 
            (/ duration unit))))
      (pitches 
        (get-pitches obj 
          (length repetitions))))
    (assert 
      (= 
        (length pitches) 
        (length repetitions)))
    (loop for p in pitches for r in repetitions for a = 
      (get-next amps) append
      (loop for i from 1 to r append
        (make-midi-note p a unit 0.25 0)))))

(defmethod get-thunder 
  (
    (obj bass-player) len start-dur end-dur start-amp start-pedal)
  (let* 
    (
      (pitches 
        (do-lookup 
          (lfl-thunder obj) 1 len)))
    (fade-over pitches start-dur end-dur 17 127 0.3 0.5 start-pedal 127)))
