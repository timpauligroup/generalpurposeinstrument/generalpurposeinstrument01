(defclass drumset-player 
  ()
  (
    (note-scale :initarg :note-scale :accessor note-scale)
    (min-dur :initarg :min-dur :accessor min-dur)
    (snare-roots :initform '
      (48 49) :accessor snare-roots)
    (kick-roots :initform '
      (24 25 26 27) :accessor kick-roots)
    (hihat-root :initform 106 :accessor hihat-root)
    (highest-note :initform 108 :accessor highest-note)))

; sounds
(defmethod get-noise 
  (
    (obj drumset-player) duration start-amp end-amp start-pitch end-pitch)
  (let* 
    (
      (pitch-obj 
        (make-rand start-pitch end-pitch)))
    (fade pitch-obj duration 
      (min-dur obj) start-amp end-amp 0.5 0.5 -1 -1)))

(defmethod get-kick 
  (
    (obj drumset-player) amp dur env-dur rest-factor)
  (let* 
    (
      (hit-dur 
        (/ env-dur 
          (length 
            (kick-roots obj))))
      (pitch-env 
        (fade-over 
          (reverse 
            (kick-roots obj)) hit-dur hit-dur amp amp 0.5 0.5 -1 -1))
      (body 
        (make-midi-note 
          (first 
            (kick-roots obj)) amp 
          (- dur env-dur) rest-factor -1)))
    (append pitch-env body)))

(defmethod get-snare 
  (
    (obj drumset-player) amp duration rest-factor)
  (make-midi-chord 
    (snare-roots obj) amp duration rest-factor -1))

(defmethod get-hihat 
  (
    (obj drumset-player) amp duration rest-factor)
  (let* 
    (
      (noise 
        (get-noise obj 
          (* duration 
            (- 1 rest-factor)) amp 1 
          (hihat-root obj) 
          (highest-note obj)))
      (rest 
        (make-rest 
          (* duration rest-factor) :duration t)))
    (append noise 
      (list rest))))

(defmethod get-cymbal 
  (
    (obj drumset-player) amp duration)
  (get-noise obj duration 1 amp 
    (hihat-root obj) 
    (highest-note obj)))

; notes
(defmethod get-hihat-pattern 
  (
    (obj drumset-player) amps duration rest-factors)
  (let* 
    (
      (unit 0.5)
      (n-hits 
        (/ duration unit)))
    (loop for i from 1 to n-hits for a = 
      (get-next amps) append 
      (get-hihat obj a unit 
        (get-next rest-factors)))))

(defmethod get-snare-pattern 
  (
    (obj drumset-player) amps duration rest-factors)
  (let* 
    (
      (unit 1)
      (n-hits 
        (/ duration unit)))
    (loop for i from 1 to n-hits for a = 
      (get-next amps) append
      (if 
        (oddp i)
        (list 
          (make-rest unit :duration t))
        (get-snare obj a unit 
          (get-next rest-factors))))))

(defmethod get-kick-pattern 
  (
    (obj drumset-player) amps duration rest-factors)
  (let* 
    (
      (unit 2)
      (n-hits 
        (/ duration unit)))
    (loop for i from 1 to n-hits for a = 
      (get-next amps) append 
      (get-kick obj a unit 0.25 
        (get-next rest-factors)))))

(defmethod get-hammers 
  (
    (obj drumset-player) dur size note-len start-amp)
  (let* 
    (
      (scale 
        (reverse 
          (every-nth-united 
            (note-scale obj) '
            (4 5))))
      (chord 
        (make-cscl 
          (list 
            (loop for i from 1 to size for s in scale collect s)))))
    (fade chord dur note-len start-amp start-amp 0.5 0.5 -1 -1)))
