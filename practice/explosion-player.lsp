(defclass explosion-player 
  ()
  (
    (note-scale :initarg :note-scale :accessor note-scale)))

(defmethod get-explosion1 
  (
    (obj explosion-player) dur size note-dur drums)
  (let* 
    (
      (snares 
        (loop for i from 1 to 4 append 
          (get-snare drums 125 0.5 0.9)))
      (len 
        (round 
          (/ 
            (length 
              (note-scale obj)) 2.0)))
      (half1 
        (every-nth 
          (subseq 
            (note-scale obj) 0 len) 3))
      (half2 
        (subseq 
          (note-scale obj) len))
      (chord1 
        (loop for i from 1 to size for h in half1 collect h))
      (chord2 
        (loop for i from 1 to size for h in half2 collect h)))
    (append snares 
      (fade 
        (make-cscl 
          (list chord1 chord2)) dur note-dur 126 126 0.8 0.2 0 126))))

(defmethod get-explosion2 
  (
    (obj explosion-player))
  (let* 
    (
      (chord 
        (every-nth 
          (note-scale obj) 2))
      (event1 
        (lambda 
          () 
          (make-midi-chord chord 126 2 0.9 0)))
      (event2 
        (make-midi-chord chord 126 0.25 0 126)))
    (append 
      (funcall event1) 
      (funcall event1) event2)))
