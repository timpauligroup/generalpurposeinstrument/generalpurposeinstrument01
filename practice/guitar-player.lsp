(defclass guitar-player 
  (string-instrument-player)
  (
    (sizes-seeder :initform 
      (make-cscl 
        (loop for i from 1 to 4 collect i)) :accessor sizes-seeder)
    (lfl-sizes :initform 
      (make-l-for-lookup 'l-sys-sizes '
        (
          (1 
            (
              (1)))
          (2 
            (
              (2)))
          (3 
            (
              (3)))
          (4 
            (
              (4)))) '
        (
          (1 
            (1 2 3 4))
          (2 
            (3 2 1))
          (3 
            (1 1 1 1 4))
          (4 
            (1)))) :accessor lfl-sizes)
    (units-seeder :initform 
      (make-cscl 
        (loop for i from 1 to 5 collect i)) :accessor units-seeder)
    (lfl-units :initform 
      (make-l-for-lookup 'l-sys-units '
        (
          (1 
            (
              (0.15)))
          (2 
            (
              (0.2) 
              (0.21)))
          (3 
            (
              (0.3) 
              (0.31) 
              (0.33)))
          (4 
            (
              (0.4) 
              (0.41) 
              (0.44) 
              (0.15)))
          (5 
            (
              (0.51) 
              (0.5)))) '
        (
          (1 
            (5 4 3 2 1))
          (2 
            (1 2 3 4))
          (3 
            (1 1 5))
          (4 
            (1))
          (5 
            (2 3 4 3)))) :accessor lfl-units)
    (durations-seeder :initform 
      (make-cscl 
        (loop for i from 1 to 5 collect i)) :accessor durations-seeder)
    (durations-funs :initform 
      (list 
        (lambda 
          (x) 
          (+ x 1))
        (lambda 
          (x) 
          (- x 2))
        (lambda 
          (x) 
          (* x 3))
        (lambda 
          (x) 
          (mod x 4))
        (lambda 
          (x) 
          (* x x))) :accessor duration-funs)
    (lfl-durations :initform 
      (make-l-for-lookup 'l-sys-durations '
        (
          (1 
            (
              (0)))
          (2 
            (
              (1)))
          (3 
            (
              (2)))
          (4 
            (
              (3)))
          (5 
            (
              (4)))
          (6 
            (
              (change)))) '
        (
          (1 
            (1 2 3 4 5 6))
          (2 
            (5 4 3 2 1))
          (3 
            (1 1 1 6))
          (4 
            (1 6))
          (5 
            (2 3 4 3))
          (6 
            (6)))) :accessor lfl-durations)))

(defun make-guitar-player 
  (scale melody)
  (make-instance 'guitar-player :lowest-note 50 :highest-note 100 :note-scale scale :degrees melody))

(defmethod get-durations 
  (
    (obj guitar-player) n)
  (let* 
    (
      (indices 
        (do-lookup 
          (lfl-durations obj) 
          (get-next 
            (durations-seeder obj)) n))
      (acc 1)
      (ischanged nil)
      (result  
        (list)))
    (loop for i in indices do
      (if 
        (eq i 'change)
        (progn
          (push acc result)
          (setf acc 1)
          (setf ischanged nil))
        (progn
          (setf acc 
            (funcall 
              (nth i 
                (duration-funs obj)) acc))
          (setf ischanged t))))
    (when ischanged
      (push acc result))
    (scale-proportions 
      (mapcar 'abs 
        (reverse result)) n)))

(defmethod get-sizes 
  (
    (obj guitar-player) n)
  (let* 
    (
      (sizes 
        (do-lookup 
          (lfl-sizes obj) 
          (get-next 
            (sizes-seeder obj)) n))) sizes))

(defmethod get-units 
  (
    (obj guitar-player) n)
  (let* 
    (
      (units 
        (do-lookup 
          (lfl-units obj) 
          (get-next 
            (units-seeder obj)) n))) units))

(defmethod get-pitches2 
  (
    (obj guitar-player) n sizes)
  (let* 
    (
      (chords 
        (apply-scale 
          (get-chords obj n sizes 1) 
          (crop-scale obj))))
    (loop for c in chords collect c)))

(defmethod get-intro 
  (
    (obj guitar-player) duration amps rest-factors)
  (let* 
    (
      (durations 
        (get-durations obj duration))
      (sizes 
        (get-sizes obj 
          (length durations)))
      (units 
        (get-units obj 
          (length durations)))
      (pitches 
        (make-cscl 
          (get-pitches2 obj 
            (length durations) 
            (loop for s in sizes collect s)))))
    (loop for d in durations for a = 
      (get-next amps) for u in units for i from 1 to 
      (length durations) append
      (if 
        (<= d 0) '
        ()
        (if 
          (oddp i)
          (fade 
            (make-cscl 
              (get-next pitches)) d u 1 a 
            (get-next rest-factors) 
            (get-next rest-factors) -1 -1)
          (make-midi-event 
            (first 
              (get-next pitches)) a d 0.1 0))))))

(defmethod get-line 
  (
    (obj guitar-player) duration amps rest-factors)
  (let* 
    (
      (unit 0.4)
      (durations 
        (get-durations obj duration))
      (sizes 
        (loop for i from 1 to 
          (length durations) collect 1))
      (pitches 
        (make-cscl 
          (get-pitches2 obj 
            (length durations) 
            (loop for s in sizes collect s)))))
    (loop for d in durations for a = 
      (get-next amps) for i from 1 to 
      (length durations) append
      (if 
        (<= d 0) '
        ()
        (if 
          (<= unit d)
          (fade 
            (make-cscl 
              (get-next pitches)) d unit a a 
            (get-next rest-factors) 
            (get-next rest-factors) -1 -1)
          (fade 
            (make-cscl 
              (get-next pitches)) d 
            (/ d 2) a a 
            (get-next rest-factors) 
            (get-next rest-factors) -1 -1))))))

(defmethod high-sine 
  (
    (obj guitar-player) dur note-len start-amp)
  (fade 
    (make-cscl 
      (list 108 107)) dur note-len start-amp 1 0.5 0.5 -1 -1))
