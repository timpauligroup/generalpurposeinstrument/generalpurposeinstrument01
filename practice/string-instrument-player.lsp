(defclass string-instrument-player 
  ()
  (
    (lowest-note :initarg :lowest-note :accessor lowest-note)
    (highest-note :initarg :highest-note :accessor highest-note)
    (note-scale :initarg :note-scale :accessor note-scale)
    (degrees :initarg :degrees :accessor degrees)))

(defmethod crop-scale 
  (
    (obj string-instrument-player))
  (let* 
    (
      (high-passed 
        (remove-if-not 
          (lambda 
            (x) 
            (<=  
              (lowest-note obj) x)) 
          (note-scale obj))))
    (remove-if-not 
      (lambda 
        (x) 
        (<=  x 
          (highest-note obj))) high-passed)))

(defmethod transform-scale 
  (
    (obj string-instrument-player) fun)
  (let* 
    (
      (result 
        (funcall fun 
          (note-scale obj))))
    (setf 
      (note-scale obj) result)))

(defmethod get-chord 
  (
    (obj string-instrument-player) degree n step)
  (let* 
    (
      (chord 
        (loop for i from 0 to 
          (- n 1) collect 
          (+ degree 
            (* step i))))) chord))

(defmethod get-chords 
  (
    (obj string-instrument-player) len sizes step)
  (let* 
    (
      (chords 
        (loop for d in 
          (loop-for-n-steps 
            (degrees obj) len) for s in sizes collect
          (get-chord obj d s step)))) chords))
