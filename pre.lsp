
(load 
    (merge-pathnames "../sc/src/all.lsp" *load-truename*))
(in-package :sc)
(load 
    (merge-pathnames "utility.lsp" *load-truename*))
(load 
    (merge-pathnames "theory/scales.lsp" *load-truename*))

; same scales as main.lsp
(defvar +root-note+ 24)
(defvar +scale1+ 
    (scale1 +root-note+))
(defvar +scale2+ 
    (scale2 +root-note+))

(defvar +rhy-chain+ 
    (make-rthm-chain 'pre-rhy-chain 64 '
        (
            (
                (
                    (8) 8)
                ( { 3 
                    (12) 
                    (12) 12 } )
                (   
                    (16)
                    (16) 
                    (16) 16)
                ( { 5 
                    (20) 
                    (20) 
                    (20) 
                    (20) 20 } )
                (   
                    (16) 16+16+16))
            (
                ( { 3 
                    (12) 12+12 } )
                (   
                    (16) 
                    (16) 16+16)
                ( { 5 
                    (20)
                    (20) 
                    (20) 20+20 } )
                ( { 5 
                    (20) 
                    (20) 20+20+20 } )
                ( { 5 
                    (20) 20+20+20+20 } ))) '
        (
            (
                (
                    (
                        (4) 4)
                    ( { 3 
                        (6) 
                        (6) 6 } )
                    (
                        (8)
                        (8) 
                        (8) 8)
                    ( { 5
                        (10)
                        (10)
                        (10) 
                        (10) 10 } )
                    ( { 5 
                        (10) 10+10+10+10 } ))
                (
                    ( { 3 
                        (6) 6+6 } )
                    (   
                        (8) 
                        (8) 8+8)
                    ( { 5 
                        (10)
                        (10) 
                        (10) 10+10 } )
                    (   
                        (8) 8+8+8)
                    ( { 5 
                        (10)
                        (10) 10+10+10 } )))
            (
                (
                    (
                        (4)
                        (8) 4+8)
                    (  
                        (4) 
                        (4) 4)
                    (  
                        (8)
                        (16)
                        (8) 
                        (16)
                        (8) 
                        (16) 8+16)
                    ( { 5
                        (10)
                        (20)
                        (10)
                        (20)
                        (10)
                        (20)
                        (10)
                        (20) 10+20 } )
                    (  
                        (8)
                        (8)
                        (8) 
                        (8) 
                        (8) 8 ))
                (
                    (4+8+4+8)
                    (  
                        (4) 4+4)
                    (  
                        (8)
                        (16)
                        (8) 
                        (16) 8+16+8+16)
                    ( { 5
                        (10)
                        (20)
                        (10)
                        (20)
                        (10)
                        (20) 10+20+10+20 } )
                    (  
                        (8) 
                        (8)
                        (8) 
                        (8) 8+8 )))) :players '
        (piano-1 piano-2) :activity-curve  
        (list 0 1 9 10 99 2) :do-rests NIL :do-sticking NIL :harmonic-rthm-curve 
        (list 0 1 9 10 99 2) :split-data NIL))

(create-psps 
    (palette +rhy-chain+) :pitch-seqs-per-rthm-seq 1 :selection-fun-data '
    (
        (1 
            (
                (24)
                (
                    (7)) 
                (23) 
                (
                    (4)) 
                (22)
                (
                    (3))
                (21)
                (
                    (1))))
        (2 
            (
                (24 
                    (7)) 
                (24 
                    (4)) 
                (24 
                    (3)) 
                (24 
                    (1))))
        (4 
            (
                (24 
                    (7) 24 
                    (7)) 
                (24 
                    (4) 23 
                    (4)) 
                (24 
                    (3) 22 
                    (3)) 
                (24 
                    (1) 21 
                    (1))))))

; a piano
(defvar +ensemble+ '
    (
        (
            (piano-1 
                (piano :midi-channel 1))
            (piano-2 
                (piano :midi-channel 1)))))

; this function distributes a given list of pitch-set-names to a new list of a given length
; the pitch-set-names get equally spaced distributed
; if that is not possible, the last name gets a bit more often repeated 
(defun create-pitch-set-map 
    (num-rthm-seqs pitch-set-names)
    (let* 
        (
            (split-length 
                (floor
                    (/ num-rthm-seqs 
                        (length pitch-set-names))))
            (split-lists
                (split-into-sub-groups3 
                    (ml 1 num-rthm-seqs) split-length)) 
            (result 
                (loop for l in split-lists for ps in pitch-set-names append
                    (ml ps 
                        (length l))))) 
        (if 
            (< 
                (length result) num-rthm-seqs) 
            (append result 
                (ml 
                    (first 
                        (last pitch-set-names))
                    (- num-rthm-seqs 
                        (length result)))) result)))

(defvar +pitch-set-map+ 
    (list 
        (list 1 
            (create-pitch-set-map 
                (num-rthm-seqs +rhy-chain+)
                (list 'pitch-set-13 'pitch-set-12 'pitch-set-11 'pitch-set-2)))))

(defvar +pitch-set-pal+ 
    (make-set-palette 'set-pal `
        (
            (pitch-set-11 
                (, 
                    (mapcar 'midi-to-note +scale1+)))
            (pitch-set-12 
                (,
                    (mapcar 'midi-to-note
                        (every-nth +scale1+ 2))))
            (pitch-set-13 
                (,
                    (mapcar 'midi-to-note
                        (every-nth +scale1+ 3))))
            (pitch-set-2 
                (,
                    (mapcar 'midi-to-note +scale2+))))))

; below a certain pitch we want only octaves and fifths
(defun is-valid-bass-interval 
    (lower-note higher-note bass-split)
    (if 
        (and 
            (< 
                (pitch- higher-note bass-split) 0) 
            (and 
                (/= 
                    (mod 
                        (pitch- higher-note lower-note) 7) 0) 
                (/= 
                    (mod 
                        (pitch- higher-note lower-note) 12) 0))) Nil T))

(defun transpose-up-until 
    (min-pitch input-index pitch-list step)
    (let 
        (   
            (current-index input-index))
        (loop while 
            (< 
                (pitch- 
                    (nth  current-index pitch-list) min-pitch) 0) do 
            (setf current-index 
                (+ current-index step)))
        (nth  current-index pitch-list)))

(defun chord-func 
    (curve-num index pitch-list pitch-seq instrument set)
    (declare 
        (ignore set instrument pitch-seq))
    (let* 
        (
            (bass-split 
                (make-pitch 'c2))
            (start-index index)
            (end-index 
                (- 
                    (length pitch-list) 1))
            (result
                (loop for before-i from start-index to end-index by curve-num for current-i from
                    (+ start-index curve-num) to end-index by curve-num collect
                    (let* 
                        (
                            (before
                                (nth before-i pitch-list))
                            (current 
                                (nth current-i pitch-list)))

                        (if 
                            (not 
                                (is-valid-bass-interval before current bass-split))
                            (transpose-up-until bass-split current-i pitch-list 3) current)))))
        (make-chord 
            (append 
                (list 
                    (nth start-index pitch-list)) result))))

(set-slot 'chord-function 'chord-func 'piano +slippery-chicken-standard-instrument-palette+)

(defvar +limits-low+ '
    (
        (piano-1
            (0 c4 9 c5 99 c6))
        (piano-2 
            (0 c0 9 c0 99 c0))))

(defvar +limits-high+ '
    (
        (piano-1 
            (0 c8 9 c8 99 c8))
        (piano-2 
            (0 c4 9 c5 99 c6))))

(make-slippery-chicken
'+pre+
:title
"pre"
:ensemble
+ensemble+
:set-map
+pitch-set-map+
:rthm-seq-map
+rhy-chain+
:set-palette
+pitch-set-pal+
:rthm-seq-palette
    (palette +rhy-chain+)
:set-limits-low
+limits-low+
:set-limits-high
+limits-high+
:tempo-map'
    (
        (1 
            (q 250)))
:avoid-melodic-octaves
Nil
:fast-leap-threshold
0.1
:pitch-seq-index-scaler-min
0)

(midi-play +pre+ :midi-file "build/pre.mid" :auto-open Nil)