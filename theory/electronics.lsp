
(defun dur-divide 
    (duration unit)
    (let* 
        (
            (n-hits 
                (floor 
                    (/ duration unit)))
            (durs 
                (loop for i from 1 to n-hits collect unit)))
        (assert 
            (<= unit duration))
        (fill-up-last durs duration)))

(defun fade-over 
    (pitches start-dur end-dur start-amp end-amp start-rest end-rest start-pedal end-pedal)
    (let* 
        (
            (n-hits 
                (length pitches)))
        (if 
            (<= n-hits 1)
            (make-midi-event pitches start-amp start-dur start-rest start-pedal)
            (let* 
                (
                    (durs 
                        (logarithmic-steps start-dur end-dur n-hits))
                    (amps 
                        (logarithmic-steps start-amp end-amp n-hits))
                    (rests 
                        (logarithmic-steps start-rest end-rest n-hits))
                    (pedals 
                        (logarithmic-steps start-pedal end-pedal n-hits)))
                (make-midi-events pitches amps durs rests pedals)))))

(defun fade 
    (pitch-obj dur dur-unit start-amp end-amp start-rest end-rest start-pedal end-pedal)
    (let* 
        (
            (durs 
                (dur-divide dur dur-unit))
            (n-hits 
                (length durs)))
        (if 
            (<= n-hits 1)
            (make-midi-event 
                (get-next pitch-obj) start-amp 
                (first durs) start-rest start-pedal)
            (let* 
                (
                    (pitches 
                        (loop for i from 1 to n-hits collect 
                            (get-next pitch-obj)))
                    (amps 
                        (logarithmic-steps start-amp end-amp n-hits))
                    (rests 
                        (logarithmic-steps start-rest end-rest n-hits))
                    (pedals 
                        (logarithmic-steps start-pedal end-pedal n-hits)))
                (make-midi-events pitches amps durs rests pedals)))))
