
(defun add-pedal 
    (event vel)
    (progn
        (push 
            (list 1 64 vel) 
            (midi-control-changes event)) event))

(defun make-event-with-rest 
    (n amp whole-dur rest-factor pedal)
    (let* 
        (
            (rest-dur 
                (* whole-dur rest-factor))
            (result 
                (list 
                    (make-event n 
                        (- whole-dur rest-dur) :amplitude 
                        (round amp) :duration t))))
        (when 
            (< 0 rest-dur)
            (nconc result 
                (list 
                    (make-rest rest-dur :duration t))))
        (when 
            (<= 0 pedal)
            (loop for r in result do 
                (add-pedal r 
                    (round pedal)))) result))

(defun make-midi-note 
    (midi-pitch amp whole-dur rest-factor pedal)
    (make-event-with-rest 
        (midi-to-pitch midi-pitch) amp whole-dur rest-factor pedal))

(defun make-midi-chord 
    (midi-chord amp whole-dur rest-factor pedal)
    (make-event-with-rest 
        (loop for n in midi-chord collect 
            (midi-to-note n)) amp whole-dur rest-factor pedal))

(defun make-midi-event 
    (midi amp whole-dur rest-factor pedal)
    (if 
        (listp midi) 
        (make-midi-chord midi amp whole-dur rest-factor pedal) 
        (make-midi-note midi amp whole-dur rest-factor pedal)))

(defun make-midi-events 
    (pitches amps whole-durs rest-factors pedals)
    (loop for p in pitches for d in whole-durs for a in amps for ped in pedals for r in rest-factors append
        (make-midi-event p a d r ped)))

(defun duration-of-events 
    (events)
    (reduce '+ 
        (loop for e in events collect 
            (duration e))))

(defun make-pedal-rest 
    (dur pedal)
    (add-pedal 
        (make-rest dur :duration t) pedal))
