(defclass rand 
  ()
  (
    (lowest :initarg :lowest :accessor lowest)
    (highest :initarg :highest :accessor highest)))

(defmethod get-next 
  (
    (obj rand))
  (+ 
    (lowest obj) 
    (random 
      (- 
        (1+ 
          (highest obj)) 
        (lowest obj)))))

(defun make-rand 
  (lowest highest)
  (make-instance 'rand :lowest lowest :highest highest))
