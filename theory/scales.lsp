;scales are just list of midi numbers
(defvar +highest-note+ 108)
(defvar +base-notes+ '
    (0 2 3 4 5 7 8 9 10))

;c d dis e f g gis a ais
(defun pitches 
    (root-note)
    (let* 
        (
            (shift 
                (lambda 
                    (x) 
                    (+ x root-note)))
            (shifted-notes 
                (mapcar shift +base-notes+)))
        (loop for i from root-note to +highest-note+ if
            (member 
                (funcall shift 
                    (mod i 12)) shifted-notes) collect i)))

(defun every-nth-united 
    (pitches indices)
    (sort 
        (reduce 'union 
            (loop for i in indices collect 
                (every-nth pitches i))) '<))

;every 4th and 5th note of pitches
(defun scale1 
    (root-note)
    (let* 
        (
            (pitches 
                (pitches root-note)))
        (every-nth-united pitches '
            (4 5 6 7))))

;cis fis h
(defun scale2 
    (root-note)
    (let* 
        (
            (negatives 
                (pitches root-note)))
        (loop for i from root-note to +highest-note+ unless
            (member i negatives) collect i)))

;not scale1 and not scale2 but pitches
(defun scale3 
    (root-note)
    (let* 
        (
            (negatives 
                (scale1 root-note))
            (pitches 
                (loop for i in 
                    (pitches root-note) unless 
                    (member i negatives) collect i))) pitches))

(defun apply-scale 
    (degrees scale)
    (loop for d in degrees collect
        (if 
            (listp d) 
            (apply-scale d scale) 
            (nth d scale))))

;operations
(defun swap 
    (scale i j)
    (let* 
        (
            (e_i 
                (nth i scale))
            (e_j 
                (nth j scale)))
        (setf 
            (nth i scale) e_j)
        (setf 
            (nth j scale) e_i)) scale)

;swap-every-nth
(defun swap-every-nth 
    (scale n)
    (let* 
        (
            (result 
                (copy-list scale))
            (i 0))
        (loop while 
            (< 
                (+ i n) 
                (length result)) do
            (swap result i 
                (+ i n))
            (setf i 
                (+ i 
                    (* 2 n)))) result))

;swap-if
(defun swap-if 
    (scale fun)
    (let* 
        (
            (result 
                (copy-list scale))
            (i 0)
            (j 0))
        (loop while 
            (< i 
                (length result)) do
            (when 
                (funcall fun 
                    (nth i result))
                (setf j 
                    (+ i 1))
                (loop while 
                    (< j 
                        (length result)) do
                    (when 
                        (funcall fun 
                            (nth j result))
                        (swap result i j)
                        (setf i j)
                        (setf j 
                            (length result)))
                    (incf j)))
            (incf i)) result))

;reverse-from-to
(defun reverse-from-to 
    (scale start end)
    (let* 
        (
            (part1 
                (subseq scale 0 start))
            (part2 
                (subseq scale start end))
            (part3 
                (subseq scale end))
            (result 
                (append part1 
                    (reverse part2) part3)))
        (assert 
            (= 
                (length scale) 
                (length result))) result))

;mirror-at
(defun mirror-at 
    (scale center)
    (let* 
        (
            (half1 
                (subseq scale 0 center))
            (half2 
                (subseq scale 
                    (+ center 1)))
            (len 
                (min 
                    (length half1) 
                    (length half2)))
            (start 
                (- center len))
            (end 
                (+ 
                    (+ center len) 1))
            (result 
                (reverse-from-to scale start end)))
        (assert 
            (= 
                (length scale) 
                (length result))) result))
