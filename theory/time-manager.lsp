(defclass time-manager 
  ()
  (
    (events :initform '
      () :accessor events)))

(defmethod find-highest-end-time 
  (
    (obj time-manager))
  (loop for e in 
    (events obj) maximize
    (end-time e)))

; list of lists means parallel
(defmethod add-events 
  (
    (obj time-manager) events-list)
  (let* 
    (
      (time 
        (find-highest-end-time obj))
      (new-events 
        (loop for events in events-list append 
          (events-update-time events :start-time time))))
    (setf 
      (events obj) 
      (append 
        (events obj) new-events))))
